from sqlalchemy import create_engine
connection_str = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'.format(
    user = 'postgres',
    password = 'Armorica_123',
    host= 'localhost',
    port='5432',
    db_name='ml_data'
)

connection_str_remote = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'.format(
    user = 'ml',
    password = 'Armorica_123',
    host= '54.209.229.233',
    port='5432',
    db_name='ml_data'
)
engine = create_engine(connection_str)

def get_engine():
    return engine

def get_remote_engine():
    return create_engine(connection_str_remote)
