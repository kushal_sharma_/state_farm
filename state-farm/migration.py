from sqlalchemy import create_engine

from config import get_engine, get_remote_engine

from Base import Base
import models

Base.metadata.create_all(get_remote_engine())
Base.metadata.create_all(get_engine())
