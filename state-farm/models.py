from Base import Base

from sqlalchemy import Column, Integer, Text
class Data(Base):
    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.X = kwargs['X']
        self.Y = kwargs['Y']
    __tablename__ = "data_x_y"
    id = Column(Integer, primary_key = True)
    X = Column(Text)
    Y = Column(Text)
