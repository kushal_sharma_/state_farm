from models import Data

from sqlalchemy.orm import sessionmaker

from config import get_engine, get_remote_engine
import copy

LocalSession = sessionmaker(bind = get_engine())
local_session = LocalSession()

RemoteSession = sessionmaker(bind = get_remote_engine())
remote_session = RemoteSession()

for id in range(629, 703):
    print "processing id = {0}".format(id)
    data = local_session.query(Data).filter(Data.id == id).all()[0]
    print data.id

    assert data is not None
    remote_data = Data(id = data.id, X= data.X, Y = data.Y)

    remote_session.add(remote_data)
    remote_session.commit()
