from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD

from sqlalchemy.orm import sessionmaker

import vgg_16
import models
import config
import json
import numpy as np

model_weights_path = '/Users/kushalsharma/Development/weights/vgg16_weights.h5'
model_save_path = '/Users/kushalsharma/Development/state-farm/models/vgg1.h5'
nb_epochs = 1

dx = 224
dy = 224
dimX = 256
dimY = 256

model = vgg_16.VGG_16(weights_path = model_weights_path)

datagen = ImageDataGenerator(
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')

model.compile(optimizer=SGD(lr = 1e-5, momentum = 0.9),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

engine = config.get_remote_engine()
Session = sessionmaker(bind = engine)
session = Session()

for epoch in range(0, nb_epochs):
    print "Training epoch {0}".format(epoch)

    for id in range(1, 703):
        print "Downloading id = {0}".format(id)
        data = session.query(models.Data).filter(models.Data.id == id).all()[0]
        x = np.array(json.loads(data.X)['val'])
        y = np.array(json.loads(data.Y)['val'])

        print "Processing id = {0}".format(id)
        cnt = 0
        print "Training batch .."
        for X_batch, Y_batch in datagen.flow(x, y):
            if cnt > 20:
                break

            else:
                xi = 0
                yi = 0
                while xi+dx < dimX:
                    while yi + dx < dimY:
                        X_crop = X_batch[0:, 0:, xi:xi+dx, yi:yi+dx]

                        # Train model on shape (32, 3, 224,224)
                        model.train_on_batch(X_crop, Y_batch)
                        yi = yi + 1
                    xi = xi + 1
                cnt = cnt + 1

model.save_weights(model_save_path)
        






