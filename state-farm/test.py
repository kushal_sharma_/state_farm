from sqlalchemy.orm import sessionmaker

import json
import numpy as np

from models import Data
from config import get_engine

Session = sessionmaker(bind = get_engine())
session = Session()

for data in session.query(Data).all():
    y = np.array(json.loads(data.X)['val'])
    print "\n"
