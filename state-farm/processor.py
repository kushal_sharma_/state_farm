from keras.preprocessing.image import load_img, img_to_array, ImageDataGenerator
from sqlalchemy.orm import sessionmaker

import json

from models import Data
from config import get_engine

batch_size = 32

Session = sessionmaker(bind = get_engine())
session = Session()

data = ImageDataGenerator()
datagen = data.flow_from_directory(
    directory = "/Users/kushalsharma/Development/state-farm/data/imgs/train",
    target_size = (256, 256),
    classes = ['c0','c1', 'c2','c3','c4','c5','c6','c7','c8','c9'],
    color_mode='rgb',
    class_mode='categorical',
    save_to_dir = "/Users/kushalsharma/Development/state-farm/data/imgs/train",
    save_prefix = "resized",
    save_format = 'jpeg',
    batch_size = batch_size
)

id = 629
LIMIT = 22424
for X, Y in datagen:
    if id % 5 is 0:
        print "Now doing next 5..."

    img_x = json.dumps({'val':X.tolist()})
    img_y = json.dumps({'val':Y.tolist()})

    data = Data(id=id, X=img_x, Y=img_y)

    session.add(data)
    session.commit()

    if id > (LIMIT/batch_size + 1):
        break
    else:
        id = id +1
    



    

    
